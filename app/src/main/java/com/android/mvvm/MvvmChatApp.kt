package com.android.mvvm

import android.annotation.SuppressLint
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.annotation.RequiresApi

class MvvmChatApp : Application() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        instance = this
        createChannelNotification()
    }

    companion object {
        lateinit var instance: MvvmChatApp
        const val CHANNEL_ID = "push_notification_id"
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannelNotification() {
        @SuppressLint("WrongConstant") val channel = NotificationChannel(
            CHANNEL_ID, "PushNotification", NotificationManager.IMPORTANCE_MAX
        )
        getSystemService(NotificationManager::class.java).createNotificationChannel(channel)
    }
}