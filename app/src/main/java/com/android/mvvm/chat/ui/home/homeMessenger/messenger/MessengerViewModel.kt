package com.android.mvvm.chat.ui.home.homeMessenger.messenger

import android.app.Application
import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.android.mvvm.R
import com.android.mvvm.chat.base.BaseViewModel
import com.android.mvvm.chat.database.firebase.Message
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.fcm.*
import com.android.mvvm.chat.model.RepoRepository
import com.android.mvvm.chat.utils.Constants
import com.ankit.trendinggit.model.Room
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import kotlin.collections.ArrayList

class MessengerViewModel constructor(private val savedStateHandle: SavedStateHandle): BaseViewModel() {
    val listMessage = MutableLiveData<ArrayList<Message>>()
    val uid = MutableLiveData<String>()
    val room = MutableLiveData<Room>()
    var sender: Users? = null
    val a: Room? = savedStateHandle.get("room")
    val myUid: String = Firebase.auth.currentUser!!.uid
    var apiService: APIService = Client.getClient("https://fcm.googleapis.com/")!!.create(
        APIService::class.java
    )


    fun getDataSender(){
        room.postValue(a)
        FirebaseDatabase.getInstance().getReference(Constants.USERS)
            .addValueEventListener(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    val user = Firebase.auth.currentUser
                    user?.let {
                        uid.postValue(it.uid)
                        FirebaseDatabase.getInstance().getReference(Constants.USERS).child(it.uid)
                            .addValueEventListener(object : ValueEventListener{
                                override fun onDataChange(snapshot: DataSnapshot) {
                                    sender = snapshot.getValue(Users::class.java)
                                }

                                override fun onCancelled(error: DatabaseError) {
                                }

                            })
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                }

            })
    }

    fun getListMessage(room: Room){
        room.id?.let {
            FirebaseDatabase.getInstance().getReference(Constants.MESSAGES).child(it)
                .addValueEventListener(object : ValueEventListener{
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val messageList = ArrayList<Message>()
                            for (dataSnapshot in snapshot.getChildren()) {
                            val message = dataSnapshot.getValue(Message::class.java)
                            if (message != null) {
                                messageList.add(message)
                            }
                        }
                        listMessage.postValue(messageList)
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }
        room.unreadMessage = 0
        val user = Firebase.auth.currentUser
        user?.let {
            Log.d("id_Sender", it.uid)
            room.id?.let { it1 ->
                FirebaseDatabase.getInstance().getReference(Constants.ROOMS).child(it.uid)
                    .child(it1).setValue(room)
            } }
    }

    private fun sendNotification(message: String, context: Context?, room: Room) {
        FirebaseDatabase.getInstance()
            .getReference(Constants.TOKENS)
            .orderByKey()
            .equalTo(room.receiver)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    for (dataSnapshot in snapshot.children) {
                        val token = dataSnapshot.getValue(Token::class.java)
                        val data = Data(
                            myUid,
                            R.mipmap.ic_launcher,
                            sender!!.username.toString() + " : " + message,
                            context!!.getString(R.string.mess),
                            room.receiver!!
                        )
                        assert(token != null)
                        val sender = Sender(data, token!!.token)
                        apiService.sendNotifications(sender)
                            ?.enqueue(object : Callback<MyResponse?> {
                                override fun onResponse(
                                    call: Call<MyResponse?>,
                                    response: Response<MyResponse?>
                                ) {
                                    if (response.code() == 200) {
                                        assert(response.body() != null)
                                    }
                                }

                                override fun onFailure(call: Call<MyResponse?>, t: Throwable) {}
                            })
                    }
                }

                override fun onCancelled(error: DatabaseError) {}
            })
    }

    fun sendMessage(message: String, room: Room, context: Context?){
        var mess: Message = Message(sender?.id, message, RepoRepository.getInstance().getCurrentTime())
        room.id?.let { FirebaseDatabase.getInstance().getReference(Constants.MESSAGES).child(it).push().setValue(mess) }
        updateDataChatRoomSender(message, room)
        updateDataChatRoomReceiver(message, room)
        sendNotification(message, context, room)
    }

    private fun updateDataChatRoomSender(message: String, room: Room){
        room.lastMessage = message
        room.timeStamp = RepoRepository.getInstance().getTimeStamp()
        sender?.id?.let { room.id?.let { it1 ->
            FirebaseDatabase.getInstance().getReference(Constants.ROOMS).child(it)
                .child(it1).setValue(room)
        } }
    }
    private fun updateDataChatRoomReceiver(message: String, room: Room){
        room.receiver?.let {
            room.id?.let { it1 ->
                FirebaseDatabase.getInstance().getReference(Constants.ROOMS)
                    .child(it)
                    .child(it1)
                    .addListenerForSingleValueEvent(object: ValueEventListener{
                        override fun onDataChange(snapshot: DataSnapshot) {
                            var roomRec: Room? = snapshot.getValue(Room::class.java)
                            if (roomRec != null){
                                roomRec.lastMessage = message
                                room.thumbnail = sender?.imageURL
                                val time: Int = roomRec.unreadMessage + 1
                                roomRec.unreadMessage = time
                                snapshot.ref.setValue(roomRec)
                            }
                        }

                        override fun onCancelled(error: DatabaseError) {
                            TODO("Not yet implemented")
                        }

                    })
            }
        }
    }
    fun sendImage(list: ArrayList<String>, room: Room, context: Context?){
        val reference = FirebaseStorage.getInstance().reference
        for (i in list.indices) {
            val file = Uri.fromFile(File(list.get(i)))
            val riversRef = reference.child("images/" + file.lastPathSegment)
            riversRef.putFile(file).addOnSuccessListener {
                riversRef.downloadUrl.addOnSuccessListener { uri ->
                    val newMessage = Message(sender?.id, uri.toString(), RepoRepository.getInstance().getCurrentTime())
                    room.id?.let { FirebaseDatabase.getInstance().getReference(Constants.MESSAGES).child(it).push().setValue(newMessage) }
                    if (context != null) {
                        updateDataChatRoomSender(context.getString(R.string.your_send), room)
                    }
                    updateDataChatRoomReceiver(sender?.username + R.string.send_img, room)
                }
            }
        }
        context?.let { sendNotification(it.getString(R.string.send_img), context, room) }
    }

    fun sendSticker(name: String, room: Room, context: Context?){
        val newMessage = Message(sender?.id, name, RepoRepository.getInstance().getCurrentTime())
        room.id?.let { FirebaseDatabase.getInstance().getReference(Constants.MESSAGES).child(it).push().setValue(newMessage) }
        updateDataChatRoomSender(R.string.your_send.toString(), room)
        updateDataChatRoomReceiver(sender?.username + context?.getString(R.string.send_img), room)
        context?.let { sendNotification(it.getString(R.string.send_img), context, room) }
    }
}