package com.android.mvvm.chat.ui.register

import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.android.mvvm.chat.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class RegisterViewModel: BaseViewModel() {
    val resultData = MutableLiveData<Boolean>(false)
    fun register(email: String, pass: String, username: String){
        val auth: FirebaseAuth = Firebase.auth
        auth.createUserWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = Firebase.auth.currentUser
                    user?.let {
                        val uid = user.uid
                        val hashMap:HashMap<String,String> = HashMap<String,String>()
                        hashMap.put(Constants.ID_RES, uid)
                        hashMap.put(Constants.USER_NAME_RES, username)
                        hashMap.put(Constants.IMG_RES, Constants.DEFAULT)
                        FirebaseDatabase.getInstance().getReference(Constants.USERS).child(uid).setValue(hashMap).addOnCompleteListener {
                            task ->
                            if(task.isSuccessful){
                                login(email, pass)
                            }
                        }
                    }
                } else {

                }
            }
    }
    fun login(email: String, pass: String){
        val auth: FirebaseAuth = Firebase.auth
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    resultData.postValue(true)
                } else {
                    resultData.postValue(false)
                }
            }
    }
}