package com.android.mvvm.chat.ui.home.homefriend.tabrequest

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.R
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.utils.Constants
import com.android.mvvm.databinding.ItemTabRequestAgreeBinding
import com.bumptech.glide.Glide
import com.daimajia.swipe.SwipeLayout

class ReceiverAdapter(private val context: Context, private var usersList: List<Users>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    interface IOnClickItem {
        fun itemClick(users: Users?)
        fun deleteItem(users: Users?)
    }

    private var iOnClickItem: IOnClickItem? = null

    fun setOnClickItem(iOnClickItem: IOnClickItem?) {
        this.iOnClickItem = iOnClickItem
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(usersList: List<Users>) {
        this.usersList = usersList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ReceiverViewHolder {
        return ReceiverViewHolder(
            ItemTabRequestAgreeBinding.inflate(
                LayoutInflater.from(viewGroup.context), viewGroup, false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        val users: Users = usersList[i]
        val receiverViewHolder = viewHolder as ReceiverViewHolder
        receiverViewHolder.bindView(users, iOnClickItem, context)
    }

    override fun getItemCount(): Int {
        return usersList.size
    }

    class ReceiverViewHolder(val binding: ItemTabRequestAgreeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindView(users: Users, iOnClickItem: IOnClickItem?, context: Context) {
            val typeBold = ResourcesCompat.getFont(context, R.font.lato_bold)
            binding.txtUsername.typeface = typeBold
            binding.txtUsername.text = users.username
            if (users.imageURL.equals(Constants.DEFAULT)) {
                Glide.with(context).load(R.mipmap.ic_launcher).circleCrop()
                    .into(binding.imgCircleFriend)
            } else {
                Glide.with(context).load(users.imageURL).circleCrop().into(binding.imgCircleFriend)
            }
            binding.btnAgreeRequest.setOnClickListener {
                iOnClickItem?.itemClick(users)
            }
            binding.swipeLayout.showMode = SwipeLayout.ShowMode.PullOut
            binding.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, binding.swipeLayout.findViewById(R.id.layout_disagree))
            binding.swipeLayout.setOnClickListener {
                iOnClickItem?.deleteItem(users)
            }
        }
    }
}