package com.android.mvvm.chat.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.inflate
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.android.mvvm.R
import com.android.mvvm.chat.ui.home.homeMessenger.HomeMessengerFragment
import com.android.mvvm.chat.ui.home.homefriend.HomeFriendFragment
import com.android.mvvm.chat.ui.home.homeprofile.HomeProfileFragment
import com.android.mvvm.chat.ui.test.TestFragment
import com.android.mvvm.databinding.FragmentHomeBinding
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeFragment: Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@HomeFragment).get(HomeViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViewPager()
        setUpBottom()
        setUpObserver()
    }

    private fun setUpObserver() {
        viewModel.updateToken()
        viewModel.getCountMess()
        viewModel.countMess.observe(viewLifecycleOwner, Observer {
            if (binding.bottomNavHome != null) run {
                val badgeDrawable: BadgeDrawable =
                    binding.bottomNavHome.getOrCreateBadge(R.id.action_messenger)
                if (it > 0) {
                    badgeDrawable.isVisible = true
                    badgeDrawable.number = it
                } else {
                    badgeDrawable.isVisible = false
                }
            }
        })

        viewModel.getCountFriend()
        viewModel.countFriend.observe(viewLifecycleOwner, Observer {
            if (binding.bottomNavHome != null) run {
                val badgeDrawable: BadgeDrawable =
                    binding.bottomNavHome.getOrCreateBadge(R.id.action_friend)
                if (it > 0) {
                    badgeDrawable.isVisible = true
                    badgeDrawable.number = it
                } else {
                    badgeDrawable.isVisible = false
                }
            }
        })
    }

    private fun setUpBottom() {
        binding.bottomNavHome.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_messenger -> binding.viewPager.currentItem = 0
                R.id.action_friend -> binding.viewPager.currentItem = 1
                R.id.action_profile -> binding.viewPager.currentItem = 2
            }
            true
        })
    }

    private fun setUpViewPager() {
        val viewPagerAdapter = ViewPagerAdapter(childFragmentManager)
        val homeMessgenger = HomeMessengerFragment()
        viewPagerAdapter.addFragment(homeMessgenger)
        viewPagerAdapter.addFragment(HomeFriendFragment())
        viewPagerAdapter.addFragment(HomeProfileFragment())
        binding.viewPager.adapter = viewPagerAdapter
        //binding.viewPager.setOffscreenPageLimit(viewPagerAdapter.count)
        binding.viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {}
            override fun onPageSelected(i: Int) {
                when (i) {
                    0 -> binding.bottomNavHome.menu.findItem(R.id.action_messenger).isChecked =
                        true
                    1 -> binding.bottomNavHome.menu.findItem(R.id.action_friend).isChecked =
                        true
                    2 -> binding.bottomNavHome.menu.findItem(R.id.action_profile).isChecked =
                        true
                }
            }

            override fun onPageScrollStateChanged(i: Int) {}
        })
    }
}