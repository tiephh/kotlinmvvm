package com.android.mvvm.chat.ui.home.homeMessenger.messenger

import com.android.mvvm.chat.fcm.MyResponse
import com.android.mvvm.chat.fcm.Sender
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface APIService {
    @Headers(
        "Content-Type:application/json",
        "Authorization:key=AAAAMxTI5uk:APA91bEUfcRb_0ze397WrcPza45bcbxolXH_gkjli0UEFxkw_GgW4NF-GLQWCCDjaSIsm03Q7r5VSd3AAnF8DRYpD8JmFnWwlgwJFtqAHVbUh434vO8s7AoDg4LZFlCS7N2fFQRP7g2V"
    )
    @POST("fcm/send")
    fun sendNotifications(@Body body: Sender?): Call<MyResponse?>?
}