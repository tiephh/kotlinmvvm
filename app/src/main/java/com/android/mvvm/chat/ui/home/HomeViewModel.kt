package com.android.mvvm.chat.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.fcm.Token
import com.android.mvvm.chat.utils.Constants
import com.ankit.trendinggit.model.Room
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import java.util.ArrayList

class HomeViewModel: BaseViewModel() {
    val countMess = MutableLiveData<Int>()
    val countFriend = MutableLiveData<Int>()

    fun getCountMess(){
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance().getReference(Constants.ROOMS).child(it.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val rooms: ArrayList<Room> = ArrayList<Room>()
                        for (dataSnapshot in snapshot.children) {
                            val room: Room? = dataSnapshot.getValue(Room::class.java)
                            if (room != null && !room.lastMessage.equals("")) {
                                rooms.add(room)
                            }
                        }
                        var count = 0
                        for (i in rooms.indices) {
                            if (rooms[i].unreadMessage > 0) {
                                count++
                            }
                        }
                        countMess.postValue(count)
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }
    }

    fun getCountFriend(){
        val userArrayList: ArrayList<Users> = ArrayList<Users>()
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance().getReference(Constants.FRIEND_REQUEST).child(it.uid)
                .child(Constants.REQUEST_RECEIVE)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (dataSnapshot in snapshot.children) {
                            val user: Users? = dataSnapshot.getValue(Users::class.java)
                            if (user != null) {
                                if (!user.id?.equals(it)!!) {
                                    userArrayList.add(user)
                                }
                            }
                        }
                        countFriend.postValue(userArrayList.size)
                    }
                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }
    }

    fun updateToken() {
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(
                        "tag home", "Fetching FCM registration token failed",
                        task.exception
                    )
                    return@OnCompleteListener
                }
                // Get new FCM registration token
                val token = task.result
                //Log
                Log.d("tag home", token!!)
                val firebaseUser = FirebaseAuth.getInstance().currentUser
                val databaseReference =
                    FirebaseDatabase.getInstance()
                        .getReference(Constants.TOKENS)
                val token1 = Token(token)
                databaseReference.child(firebaseUser!!.uid).setValue(token1)
            })
    }
}