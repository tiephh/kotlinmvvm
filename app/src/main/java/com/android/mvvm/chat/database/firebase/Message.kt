package com.android.mvvm.chat.database.firebase

class Message {
    var senderId: String? = null
    var content: String? = null
    var time: String? = null

    constructor() {}
    constructor(senderId: String?, content: String?, time: String?) {
        this.senderId = senderId
        this.content = content
        this.time = time
    }
}
