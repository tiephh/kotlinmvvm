package com.android.mvvm.chat.ui.home.homefriend

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentHomeFriendBinding
import com.android.mvvm.chat.ui.home.homefriend.tabfriend.TabFriendFragment
import com.android.mvvm.chat.ui.home.homefriend.tabrequest.TabRequestFragment
import com.google.android.material.badge.BadgeDrawable
import mvvm.chat.ui.home.homefriend.tabuser.TabUserFragment

class HomeFriendFragment : Fragment() {

    private lateinit var viewModel: HomeFriendViewModel
    private lateinit var binding: FragmentHomeFriendBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeFriendBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@HomeFriendFragment).get(HomeFriendViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    private var onTextChanged: OnTextChanged? = null

    interface OnTextChanged {
        fun onTextChanged(word: String?)
    }

    fun setOnTextChanged(onTextChanged: OnTextChanged?) {
        this.onTextChanged = onTextChanged
    }

    private var onTextChanged2: OnTextChanged2? = null

    interface OnTextChanged2 {
        fun onTextChanged2(word: String?)
    }

    fun setOnTextChanged2(onTextChanged2: OnTextChanged2?) {
        this.onTextChanged2 = onTextChanged2
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        setUpObserver()
    }

    private fun setUpObserver() {
        viewModel.getCountFriend()
        viewModel.countFriend.observe(viewLifecycleOwner, Observer {
            if(it >0){
                binding.txtRequestNumber.text = it.toString()
            }else{
                binding.txtRequestNumber.visibility = View.GONE
            }
        })
    }


    private fun setupView() {
        binding.searchViewHomeFriend.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                if (binding.viewPagerFriend.currentItem == 0) {
                    if (onTextChanged != null) {
                        onTextChanged!!.onTextChanged(charSequence.toString().toLowerCase())
                    }
                } else if (binding.viewPagerFriend.currentItem == 1) {
                    if (onTextChanged2 != null) {
                        onTextChanged2!!.onTextChanged2(charSequence.toString().toLowerCase())
                    }
                }
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        val adapter = ViewPagerHomeFriendAdapter(childFragmentManager)
        adapter.addFragment(TabFriendFragment(), requireContext().getString(R.string.tab_friend))
        adapter.addFragment(TabUserFragment(), requireContext().getString(R.string.tab_all))
        adapter.addFragment(TabRequestFragment(), requireContext().getString(R.string.tab_request))
        binding.viewPagerFriend.adapter = adapter
        binding.tabLayoutFriend.setupWithViewPager(binding.viewPagerFriend)
    }
}