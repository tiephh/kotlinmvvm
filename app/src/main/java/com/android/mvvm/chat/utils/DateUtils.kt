package com.android.mvvm.chat.utils

import java.text.SimpleDateFormat
import java.util.*

class DateUtils(date: Date) {
    private var dateTime: Date? = null
    init {
        dateTime = date
    }
    var sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
    var monthF = SimpleDateFormat("MM")
    var dateFormat = SimpleDateFormat("dd")
    var hour = SimpleDateFormat("HH:mm")
    var day = SimpleDateFormat("dd-MM-yyyy")
    fun getMonth(): String? {
        return monthF.format(dateTime)
    }

    fun getYear(): String? {
        return day.format(dateTime)
    }

    fun getDate(): String? {
        return dateFormat.format(dateTime)
    }

    fun getHour(): String? {
        return hour.format(dateTime)
    }
}
