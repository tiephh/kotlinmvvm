package com.android.mvvm.chat.ui.home.homeprofile

import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase

class HomeProfileViewModel : BaseViewModel() {
    val resultEmail = MutableLiveData<String>()
    val resultUser = MutableLiveData<Users>()
    fun showProfile() {
        val user = Firebase.auth.currentUser
        user?.let {
            val email: String? = user.email
            resultEmail.postValue(email)
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.USERS)
                    .child(uid)
                    .addValueEventListener(object : ValueEventListener {
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            val users: Users? = dataSnapshot.getValue(Users::class.java)
                            if (users != null) {
                                resultUser.postValue(users)
                            }
                        }

                        override fun onCancelled(databaseError: DatabaseError) {}
                    })
        }
    }
    fun signOut(){
        FirebaseAuth.getInstance().signOut()
    }
}