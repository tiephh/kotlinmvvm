package com.android.mvvm.chat.model

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.model.api.ApiClient
import com.android.mvvm.chat.utils.Constants
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class RepoRepository {

    // GET repo list
    fun getRepoList(onResult: (isSuccess: Boolean, response: GitResponse?) -> Unit) {

        ApiClient.instance.getRepo().enqueue(object : Callback<GitResponse> {
            override fun onResponse(call: Call<GitResponse>?, response: Response<GitResponse>?) {
                if (response != null && response.isSuccessful)
                    onResult(true, response.body()!!)
                else
                    onResult(false, null)
            }

            override fun onFailure(call: Call<GitResponse>?, t: Throwable?) {
                onResult(false, null)
            }

        })
    }
    fun getTimeStamp(): String? {
        val tsLong = System.currentTimeMillis()
        return tsLong.toString()
    }

    fun getCurrentTime(): String? {
        val date = Calendar.getInstance().time
        val df = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        return df.format(date)
    }

    companion object {
        private var INSTANCE: RepoRepository? = null
        fun getInstance() = INSTANCE
                ?: RepoRepository().also {
                    INSTANCE = it
                }
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }
    fun getUid(): String{
        val user = Firebase.auth.currentUser
        user?.let {
            var uid:String = it.uid
            return  uid
        }
        return  "1"
    }
    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun generateRoomKey(): String? {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            return FirebaseDatabase.getInstance()
                .getReference(Constants.ROOMS)
                .child(it.uid)
                .push()
                .getKey()
        }
        return "1"
    }

}