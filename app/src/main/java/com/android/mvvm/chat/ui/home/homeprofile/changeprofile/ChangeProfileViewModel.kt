package mvvm.chat.ui.home.homeprofile.changeprofile

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.utils.Constants
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import java.util.HashMap

class ChangeProfileViewModel : BaseViewModel() {
    val resultUser = MutableLiveData<Users>()
    fun setUpView() {
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.USERS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val users: Users? = dataSnapshot.getValue(Users::class.java)
                        resultUser.postValue(users)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }

    }

    fun uploadImage(mUri: Uri) {
        val reference = FirebaseStorage.getInstance().reference
        val riversRef = reference.child("images/" + mUri.lastPathSegment)
        riversRef.putFile(mUri).addOnSuccessListener {
            riversRef.downloadUrl.addOnSuccessListener { uri ->
                val user = Firebase.auth.currentUser
                user?.let {
                    val uid: String = user.uid
                    FirebaseDatabase.getInstance().getReference(Constants.USERS)
                        .child(uid)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                val users = dataSnapshot.getValue(Users::class.java)
                                if (users != null) {
                                    users.imageURL = uri.toString()
                                    dataSnapshot.ref.setValue(users)
                                }
                            }

                            override fun onCancelled(databaseError: DatabaseError) {}
                        })
                }
            }
        }
    }

    fun uploadName(name: String) {
        val map = HashMap<String, Any>()
        map[Constants.USER_NAME] = name
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = user.uid
            FirebaseDatabase.getInstance().getReference(Constants.USERS)
                .child(uid)
                .updateChildren(map)
        }
    }
}