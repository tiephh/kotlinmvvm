package com.android.mvvm.chat.ui.home.homeprofile.changeprofile

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.android.mvvm.R
import com.android.mvvm.chat.utils.Constants
import com.android.mvvm.databinding.FragmentChangeProfileBinding
import com.bumptech.glide.Glide
import mvvm.chat.ui.home.homeprofile.changeprofile.ChangeProfileViewModel
import java.util.*

class ChangeProfileFragment : Fragment() {

    private lateinit var viewModel: ChangeProfileViewModel
    private lateinit var binding: FragmentChangeProfileBinding
    private val imageRequest = 1
    var mUri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChangeProfileBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@ChangeProfileFragment)
                .get(ChangeProfileViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObservers()
        setListeners()
    }

    private fun setListeners() {
        viewModel.setUpView()
        binding.imgBackHomeProfile.setOnClickListener {
            Navigation.findNavController(requireView()).navigateUp()
        }
        binding.imgCamera.setOnClickListener(View.OnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(intent, imageRequest)
        })
        binding.txtDone.setOnClickListener {
            if (mUri != null) {
                viewModel.uploadImage(mUri!!)
            }
            if (Objects.requireNonNull<Editable>(binding.edtChangeName.text).length > 1) {
                viewModel.uploadName(binding.edtChangeName.text.toString())
            }
            Navigation.findNavController(requireView()).navigateUp()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == imageRequest && resultCode == Activity.RESULT_OK && data != null && data.data != null) {
            mUri = data.data
            if (mUri != null) {
                if (context != null) {
                    if (mUri.toString() == Constants.DEFAULT)
                        Glide.with(requireContext()).load(R.mipmap.ic_launcher).into(binding.imgAva)
                    else
                        Glide.with(requireContext()).load(mUri.toString()).into(binding.imgAva)
                }
            }
        }
    }

    private fun setupObservers() {
        viewModel.resultUser.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (context != null) {
                    binding.edtChangeName.setText(it.username)
                    if (it.imageURL.equals(Constants.DEFAULT))
                        Glide.with(requireContext()).load(R.mipmap.ic_launcher).into(binding.imgAva)
                    else
                        Glide.with(requireContext()).load(it.imageURL).into(binding.imgAva)
                }
            }
        })
    }
}