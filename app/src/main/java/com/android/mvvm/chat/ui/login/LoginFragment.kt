package com.android.mvvm.chat.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.FragmentLoginBinding

class LoginFragment : Fragment(){
    private lateinit var binding: FragmentLoginBinding
    private val viewModel: LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false).apply {
//            viewModel = ViewModelProviders.of(this@LoginFragment).get(LoginViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener()
        setupObserver()
        changeColorButton()
    }
    private fun listener(){
        binding.btnLogin.setOnClickListener{
            var email : String = binding.etUserLogin.text.toString();
            var pass : String = binding.etPassLogin.text.toString();
            viewModel.login(email, pass)
        }
        binding.tvRegister.setOnClickListener{
            NavHostFragment.findNavController(this).navigate(R.id.action_register_fragment, null)
        }
    }
    private fun setupObserver(){
        viewModel.resultData.observe(viewLifecycleOwner, Observer{
            if(it == true){
                NavHostFragment.findNavController(this).navigate(R.id.action_loginFragment_to_homeFragment, null)
            }
        })
    }
    private fun changeColorButton() {
        binding.etPassLogin.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.etUserLogin.text!!.length and binding.etPassLogin.text!!.length > 0) {
                    binding.btnLogin.setBackgroundResource(R.drawable.button_custom_blue)
                } else {
                    binding.btnLogin.setBackgroundResource(R.drawable.button_custom)
                }
            }
        })
        binding.etUserLogin.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.etUserLogin.text!!.length and binding.etPassLogin.text!!.length > 0) {
                    binding.btnLogin.setBackgroundResource(R.drawable.button_custom_blue)
                } else {
                    binding.btnLogin.setBackgroundResource(R.drawable.button_custom)
                }
            }
        })
    }
}