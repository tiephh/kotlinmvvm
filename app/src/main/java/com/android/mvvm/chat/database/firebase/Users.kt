package com.android.mvvm.chat.database.firebase

import java.io.Serializable

class Users : Serializable {
    var id: String? = null
    var username: String? = null
    var imageURL: String? = null

    constructor(id: String?, username: String?, imageURL: String?) {
        this.id = id
        this.username = username
        this.imageURL = imageURL
    }

    constructor() {}
}
