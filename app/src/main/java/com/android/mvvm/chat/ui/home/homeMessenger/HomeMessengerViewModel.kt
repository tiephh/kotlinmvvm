package com.android.mvvm.chat.ui.home.homeMessenger

import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.android.mvvm.chat.utils.Constants
import com.ankit.trendinggit.model.Room
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import java.util.*

class HomeMessengerViewModel: BaseViewModel() {
    val listRoom = MutableLiveData<ArrayList<Room>>()

    fun getListChatRoom(){
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance().getReference(Constants.ROOMS).child(user.uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot){
                        val rooms = ArrayList<Room>()
                        for (dataSnapshot in dataSnapshot.getChildren()) {
                            val room = dataSnapshot.getValue(Room::class.java)
                            if (room != null && !room.lastMessage.equals("")) {
                                rooms.add(room)
                            }
                        }
                        Collections.sort(rooms, object : Comparator<Room?> {
                            override fun compare(p0: Room?, p1: Room?): Int {
                                if(p0 != null && p1 != null){
                                    return if (p0.timeStamp!!.toLong() < p1.timeStamp!!.toLong()) {
                                        1
                                    } else {
                                        if (p0.timeStamp!!.toLong() < p1.timeStamp!!.toLong()) {
                                            0
                                        } else {
                                            -1
                                        }
                                    }
                                }
                                return -1
                            }
                        })
                        listRoom.postValue(rooms)
                    }
                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }
    }
}