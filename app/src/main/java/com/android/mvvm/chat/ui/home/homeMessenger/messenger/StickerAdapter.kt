package com.android.mvvm.chat.ui.home.homeMessenger.messenger

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.databinding.ItemImageBinding
import com.android.mvvm.databinding.ItemStickerBinding
import com.bumptech.glide.Glide
import java.util.ArrayList

class StickerAdapter(private val mContext: Context, private val mListPhoto: ArrayList<String>?) :
    RecyclerView.Adapter<StickerAdapter.StickerViewHolder>() {
    interface IOnClick {
        fun itemClick(name: String?)
    }

    var iOnClick: IOnClick? = null
    fun setOnClick(iOnClick: IOnClick?) {
        this.iOnClick = iOnClick
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): StickerViewHolder {
        return StickerViewHolder(
            ItemStickerBinding.inflate(
                LayoutInflater.from(viewGroup.context), viewGroup, false
            )
        )
    }

    override fun onBindViewHolder(stickerViewHolder: StickerViewHolder, i: Int) {
        val image = mListPhoto!![i]
        stickerViewHolder.bindView(image, iOnClick)
    }

    override fun getItemCount(): Int {
        return mListPhoto?.size ?: 0
    }

    inner class StickerViewHolder(val binding: ItemStickerBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindView(image: String?, iOnClick: IOnClick?) {
            Glide.with(mContext).load(image).into(binding.ivSticker)
            binding.ivSticker.setOnClickListener { iOnClick?.itemClick(image) }
        }

        init {

        }
    }
}
