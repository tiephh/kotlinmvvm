package mvvm.chat.ui.home.homefriend.tabuser

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.mvvm.chat.ui.home.homefriend.tabuser.TabUserAdapter
import com.android.mvvm.chat.ui.home.homefriend.tabuser.TabUserViewModel
import com.android.mvvm.databinding.FragmentTabUserBinding
import com.android.mvvm.chat.ui.home.homefriend.HomeFriendFragment
import com.android.mvvm.chat.utils.listener.OnItemClickListener

class TabUserFragment : Fragment() {

    private lateinit var viewModel: TabUserViewModel
    private lateinit var binding: FragmentTabUserBinding

    private var adapter: TabUserAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabUserBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@TabUserFragment).get(TabUserViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getListFriend()
        viewModel.getDataSender()
        setupObservers()
        setListeners()
    }

    private fun setListeners() {
        initAdapter()
        (parentFragment as HomeFriendFragment?)?.setOnTextChanged2(object :
            HomeFriendFragment.OnTextChanged2 {
            override fun onTextChanged2(word: String?) {
                if (word != null) {
                    viewModel.searchUser(word)
                }
            }
        })
    }

    private fun initAdapter() {
        binding.recyclerViewTabUser.layoutManager = LinearLayoutManager(context)
        adapter = context?.let { TabUserAdapter(it, ArrayList()) }
        binding.recyclerViewTabUser.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.resultUserSort.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (adapter != null) {
                    adapter!!.setData(it)
                    adapter!!.setOnItemClickListener(object : OnItemClickListener {
                        override fun sendRequestFriend(position: Int) {
                            it[position].users?.let { it1 -> viewModel.sendRequest(it1) }
                        }

                        override fun cancelRequestFriend(position: Int) {
                            it[position].users?.let { it1 -> viewModel.cancelRequest(it1) }
                        }
                    })
                }
            }
        })
    }
}