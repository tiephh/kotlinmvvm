package com.android.mvvm.chat.ui.splash

import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase

class SplashViewModel: BaseViewModel() {
    val checkUser = MutableLiveData<Int>()
    val OPEN_LOGIN: Int = 1
    val OPEN_HOME: Int = 2
    fun checkLogin(){
        if(FirebaseAuth.getInstance().currentUser == null){
            checkUser.postValue(OPEN_LOGIN)
        }else{
            checkUser.postValue(OPEN_HOME)
        }
    }
}