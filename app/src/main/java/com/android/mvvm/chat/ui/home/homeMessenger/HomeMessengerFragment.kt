package com.android.mvvm.chat.ui.home.homeMessenger

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.mvvm.R
import com.android.mvvm.databinding.HomeMessengerFragmentBinding
import com.ankit.trendinggit.model.Room
import java.util.ArrayList


class HomeMessengerFragment: Fragment() {
    private lateinit var binding: HomeMessengerFragmentBinding
    private lateinit var viewModel: HomeMessengerViewModel
    private lateinit var adapter: RoomAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeMessengerFragmentBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@HomeMessengerFragment).get(HomeMessengerViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getListChatRoom()
        initAdapter()
        setUpObserver()
    }

    private fun setUpObserver() {
        viewModel.listRoom.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            adapter.setData(it)
        })
    }

    private fun initAdapter() {
        
        context?.let {
            adapter = RoomAdapter(it, ArrayList())
        }
        adapter.setOnItemClickListener(object: RoomAdapter.OnItemClick{
            override fun onItemClick(room: Room) {
                val bundle = Bundle()
                bundle.putSerializable("room", room)
                NavHostFragment.findNavController(
                    this@HomeMessengerFragment).navigate(R.id.homeMessengerFragment_to_messengerFragment, bundle)
            }
        })
        binding.rvMesHomeMes.layoutManager = LinearLayoutManager(context)
        binding.rvMesHomeMes.adapter = adapter
    }

}