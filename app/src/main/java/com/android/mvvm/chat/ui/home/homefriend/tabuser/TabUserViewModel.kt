package com.android.mvvm.chat.ui.home.homefriend.tabuser

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.utils.Constants
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import mvvm.chat.ui.home.homefriend.tabuser.UserSort
import java.util.*

class TabUserViewModel : BaseViewModel() {
    val resultUserSort = MutableLiveData<List<UserSort>>()

    var sender: Users? = null

    private var friends: List<Users> = ArrayList()
    private var friendRequestSends: List<Users> = ArrayList()
    private var users = ArrayList<Users>()
    private var userSorts: ArrayList<UserSort> = ArrayList()

    lateinit var user: Users

    fun getListFriend() {
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.FRIENDS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        friends = ArrayList()
                        for (snapshot in dataSnapshot.children) {
                            val user: Users? = snapshot.getValue(Users::class.java)
                            if (user != null) {
                                (friends as java.util.ArrayList<Users>).add(user)
                            }
                        }
                        getListFriendRequestSend()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    private fun getListFriendRequestSend() {
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance()
                .getReference(Constants.FRIEND_REQUEST)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        friendRequestSends = ArrayList()
                        for (snapshot in dataSnapshot.children) {
                            val user: Users? = snapshot.getValue(Users::class.java)
                            if (user != null) {
                                (friendRequestSends as java.util.ArrayList<Users>).add(user)
                            }
                        }
                        getListUser()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    private fun getListUser() {
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.USERS)
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        users = ArrayList()
                        for (snapshot in dataSnapshot.children) {
                            val user: Users? = snapshot.getValue(Users::class.java)
                            if (user != null && !user.id.equals(uid)) {
                                users.add(user)
                            }
                        }
                        sortListUserWithAlphabet()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun sortListUserWithAlphabet() {
        val sortedUsers = users.sortedBy { it.username?.first()?.toUpperCase().toString() }
        users = ArrayList(sortedUsers)
        filterListUser()
    }

    private fun filterListUser() {
        userSorts = ArrayList()
        var lastHeader = ""
        for (user in users) {
            val header: String = user.username?.first().toString().toUpperCase()
            val currentState: Constants.StateUser = getStateUser(user)
            if (header != lastHeader) {
                lastHeader = header
                userSorts.add(UserSort(header, null, true, currentState))
            }
            userSorts.add(UserSort(header, user, false, currentState))
        }

        resultUserSort.postValue(userSorts)
    }

    private fun getStateUser(user: Users): Constants.StateUser {
        var state: Constants.StateUser = Constants.StateUser.OTHERS
        for (friend in friends) {
            if (user.id.equals(friend.id)) {
                state = Constants.StateUser.FRIEND
            }
        }
        for (friendRequest in friendRequestSends) {
            if (user.id.equals(friendRequest.id)) {
                state = Constants.StateUser.REQUEST_FRIEND
            }
        }
        return state
    }

    fun getDataSender() {
        val user = Firebase.auth.currentUser
        user?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.USERS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        sender = dataSnapshot.getValue(Users::class.java)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }

    }

    private fun removeValueSender(users: Users, id: String, table: String) {
        val query: Query = FirebaseDatabase.getInstance().reference
            .child(Constants.FRIEND_REQUEST)
            .child(id)
            .child(table)
            .orderByChild(Constants.ID).equalTo(users.id)
        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (appleSnapshot in dataSnapshot.children) {
                    appleSnapshot.ref.removeValue()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    fun sendRequest(user: Users) {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().reference
                .child(Constants.FRIEND_REQUEST)
                .child(uid)
                .child(Constants.REQUEST_SEND)
                .push()
                .setValue(user)
        }
        user.id?.let {
            FirebaseDatabase.getInstance().reference
                .child(Constants.FRIEND_REQUEST)
                .child(it)
                .child(Constants.REQUEST_RECEIVE)
                .push()
                .setValue(sender)
        }
    }

    fun cancelRequest(user: Users) {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            removeValueSender(
                user,
                uid,
                Constants.REQUEST_SEND
            )
        }
        sender?.let {
            user.id?.let { it1 ->
                removeValueSender(it, it1, Constants.REQUEST_RECEIVE)
            }
        }
    }

    fun searchUser(word: String) {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.USERS)
                .orderByChild(Constants.USER_NAME)
                .startAt(word).endAt(word + "\uf8ff")
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    override fun onDataChange(snapshot: DataSnapshot) {
                        users.clear()
                        for (dataSnapshot in snapshot.children) {
                            val user: Users = dataSnapshot.getValue(Users::class.java)!!
                            if (!user.id.equals(uid)) {
                                users.add(user)
                            }
                        }
                        sortListUserWithAlphabet()
                    }

                    override fun onCancelled(error: DatabaseError) {}
                })
        }
    }
}