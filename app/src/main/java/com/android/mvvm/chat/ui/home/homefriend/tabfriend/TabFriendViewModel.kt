package com.android.mvvm.chat.ui.home.homefriend.tabfriend

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.model.RepoRepository
import com.android.mvvm.chat.utils.Constants
import com.ankit.trendinggit.model.Room
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import mvvm.chat.ui.home.homefriend.tabfriend.FriendSort
import java.util.HashMap

class TabFriendViewModel : BaseViewModel() {
    val resultFriendSort = MutableLiveData<List<FriendSort>>()
    val openChatRoom = MutableLiveData<Room>()

    private var userArrayList = ArrayList<Users>()
    private var users = ArrayList<Users>()
    private var sortUsers: ArrayList<FriendSort> = ArrayList()
    var sender: Users? = null

    fun readUser() {
        getDataUser()
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.FRIENDS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (snapshot in dataSnapshot.children) {
                            val user = snapshot.getValue(Users::class.java)
                            if (user != null) {
                                if (!user.id.equals(uid)) {
                                    userArrayList.add(user)
                                }
                            }
                        }
                        readUserFriend(userArrayList)
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
        }
    }

    private fun readUserFriend(userArrayList: ArrayList<Users>) {
        FirebaseDatabase.getInstance().getReference(Constants.USERS)
            .addValueEventListener(object : ValueEventListener {
                @RequiresApi(api = Build.VERSION_CODES.N)
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    users.clear()
                    for (snapshot in dataSnapshot.children) {
                        val user = snapshot.getValue(Users::class.java)
                        for (i in userArrayList.indices) {
                            if (user != null && user.id.equals(userArrayList[i].id)) {
                                users.add(user)
                            }
                        }
                    }
                    sortListUserWithAlphabet(users)
                }
                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun sortListUserWithAlphabet(users: ArrayList<Users>) {
        val sortedUsers = users.sortedBy {it.username?.first()?.toUpperCase().toString()}
        this.users = ArrayList(sortedUsers)
        filterListUser()
    }

    private fun filterListUser() {
        sortUsers = ArrayList()
        var lastHeader = ""
        for (user in users) {
            val header: String =
                java.lang.String.valueOf(user.username!!.first()).toUpperCase()
            if (header != lastHeader) {
                lastHeader = header
                sortUsers.add(FriendSort(header, null, true))
            }
            sortUsers.add(FriendSort(header, user, false))
        }
        resultFriendSort.postValue(sortUsers)
    }

    fun searchUser(word: String) {
        val userArrayList = ArrayList<Users?>()
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.FRIENDS)
                .child(uid)
                .addValueEventListener(object : ValueEventListener {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (dataSnapshot in snapshot.children) {
                            val user = dataSnapshot.getValue(Users::class.java)!!
                            if (!user.id.equals(uid)) {
                                userArrayList.add(user)
                            }
                        }
                        readUserSearch(userArrayList, word)
                    }
                    override fun onCancelled(error: DatabaseError) {}
                })
        }
    }

    private fun readUserSearch(userArrayList: ArrayList<Users?>, word: String) {
        FirebaseDatabase.getInstance().getReference(Constants.USERS)
            .orderByChild(Constants.USER_NAME)
            .startAt(word).endAt(word + "\uf8ff")
            .addValueEventListener(object : ValueEventListener {
                @RequiresApi(api = Build.VERSION_CODES.N)
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    users.clear()
                    for (snapshot in dataSnapshot.children) {
                        val user = snapshot.getValue(Users::class.java)
                        for (i in userArrayList.indices) {
                            if (user != null && user.id.equals(userArrayList[i]!!.id)) {
                                users.add(user)
                            }
                        }
                    }
                    sortListUser(users)
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private fun sortListUser(userArrayList: ArrayList<Users>) {
        val sortedUserSearch = userArrayList.sortedBy {it.username?.first()?.toUpperCase().toString()}
        this.userArrayList = ArrayList(sortedUserSearch)
        filterListUserSearch(userArrayList)
    }

    private fun filterListUserSearch(userArrayList: ArrayList<Users>) {
        val usersSortSearch: ArrayList<FriendSort> = ArrayList()
        var lastHeader = ""
        for (user in userArrayList) {
            val header: String =
                java.lang.String.valueOf(user.username!!.first()).toUpperCase()
            if (header != lastHeader) {
                lastHeader = header
                usersSortSearch.add(FriendSort(header, null, true))
            }
            usersSortSearch.add(FriendSort(header, user, false))
        }
        resultFriendSort.postValue(usersSortSearch)
    }

    fun checkExistRoom(user: Users){
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            val uid: String = it.uid
            FirebaseDatabase.getInstance().getReference(Constants.ROOMS)
                .child(uid).get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.getResult() != null) {
                        for (snapshot in task.result.children) {
                            val room = snapshot.getValue(Room::class.java)
                            if (room != null && room.receiver != null && room.receiver.equals(user.id)) {
                                openChatRoom.postValue(room)
                                return@addOnCompleteListener
                            }
                        }
                        createChatRoom(user)
                    }
                }
        }
    }

    private fun createChatRoom(user: Users) {
        val roomKey: String? = RepoRepository.getInstance().generateRoomKey()
        val newRoom = Room(
            roomKey, user.username,
            user.imageURL, user.id, 0, "", RepoRepository.getInstance().getTimeStamp()
        )
        val childUpdates: HashMap<String, Any> = HashMap()
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            childUpdates.put(it.uid + "/" + newRoom.id, newRoom.toMap());
        }
        childUpdates.put(user.id.toString() + "/" + newRoom.id, createReceiverRoom(newRoom).toMap())
        FirebaseDatabase.getInstance().getReference(Constants.ROOMS).updateChildren(childUpdates)
            .addOnSuccessListener{
                openChatRoom.postValue(newRoom)
            }
        .addOnFailureListener{

        }


    }
    private fun getDataUser() {
        val userCurrent = Firebase.auth.currentUser
        userCurrent?.let {
            FirebaseDatabase.getInstance().getReference(Constants.USERS)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val user = Firebase.auth.currentUser
                        user?.let {
                            FirebaseDatabase.getInstance().getReference(Constants.USERS)
                                .child(it.uid)
                                .addValueEventListener(object : ValueEventListener {
                                    override fun onDataChange(snapshot: DataSnapshot) {
                                        sender = snapshot.getValue(Users::class.java)
                                    }
                                    override fun onCancelled(error: DatabaseError) {
                                    }
                                })
                        }
                    }
                    override fun onCancelled(error: DatabaseError) {
                    }

                })
        }
    }

    private fun createReceiverRoom(newRoom: Room): Room {
        return Room(
            newRoom.id,
            sender?.username,
            sender?.imageURL,
            sender?.id,
            newRoom.unreadMessage,
            newRoom.lastMessage,
            newRoom.timeStamp
        )
        return newRoom
    }
}