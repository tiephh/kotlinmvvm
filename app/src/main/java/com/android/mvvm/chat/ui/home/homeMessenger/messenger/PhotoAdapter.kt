package com.android.mvvm.chat.ui.home.homeMessenger.messenger

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.databinding.ItemImageBinding
import com.bumptech.glide.Glide
import java.io.File
import java.util.ArrayList

class PhotoAdapter(private val mContext: Context, private val mListPhoto: List<String>?) :
    RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder>() {
    var listImg = ArrayList<String>()

    interface IOnClick {
        fun itemClick(name: String?)
    }

    @JvmName("getListImg1")
    fun getListImg(): ArrayList<String> {
        return listImg
    }

    fun clearList() {
        listImg.clear()
    }
    init {
        this.listImg = mListPhoto as ArrayList<String>
    }

    var iOnClick: IOnClick? = null

    fun setOnClick(iOnClick: IOnClick?) {
        this.iOnClick = iOnClick
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): PhotoViewHolder {
        return PhotoViewHolder(
            ItemImageBinding.inflate(
                LayoutInflater.from(viewGroup.context), viewGroup, false
            )
        )
    }

    override fun onBindViewHolder(photoViewHolder: PhotoViewHolder, i: Int) {
        val image = mListPhoto?.get(i)
        image?.let { photoViewHolder.bindView(it, iOnClick) }
    }

    override fun getItemCount(): Int {
        return mListPhoto?.size ?: 0
    }

    inner class PhotoViewHolder(val binding:ItemImageBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindView(image: String, iOnClick: IOnClick?) {
            Glide.with(mContext).load(File(image)).into(binding.ivGallery)
            binding.ivGallery.setOnClickListener {
                if (binding.tvClick.visibility == View.VISIBLE) {
                    binding.tvClick.visibility = View.GONE
                } else {
                    binding.tvClick.visibility = View.VISIBLE
                }
                if (listImg.contains(image)) {
                    listImg.remove(image)
                } else {
                    listImg.add(image)
                    binding.tvClick.text = "" + listImg.size + 1
                }
                iOnClick?.itemClick(image)
            }
        }

    }
}
