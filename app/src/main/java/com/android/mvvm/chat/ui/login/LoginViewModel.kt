package com.android.mvvm.chat.ui.login

import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginViewModel : BaseViewModel() {
    val resultData = MutableLiveData<Boolean>(false)

    fun login(email: String, pass: String){
        val auth: FirebaseAuth = Firebase.auth
        auth.signInWithEmailAndPassword(email, pass)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    resultData.postValue(true)
                } else {
                    resultData.postValue(false)
                }
            }
    }
}