package com.android.mvvm.chat.ui.home.homefriend.tabfriend

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.mvvm.R
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.databinding.FragmentTabFriendBinding
import com.android.mvvm.chat.ui.home.homefriend.HomeFriendFragment
import java.util.ArrayList

class TabFriendFragment : Fragment() {

    private lateinit var viewModel: TabFriendViewModel
    private lateinit var binding: FragmentTabFriendBinding

    private var adapter: TabFriendAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabFriendBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@TabFriendFragment).get(TabFriendViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObservers()
        setListeners()
    }

    private fun setListeners() {
        setInitAdapter()
        viewModel.readUser()
        val homeFriendFragment = parentFragment as HomeFriendFragment?
        homeFriendFragment?.setOnTextChanged(object : HomeFriendFragment.OnTextChanged {
            override fun onTextChanged(word: String?) {
                if (word != null) {
                    viewModel.searchUser(word)
                }
            }
        })
    }

    private fun setInitAdapter() {
        binding.recyclerViewTabFriend.setHasFixedSize(true)
        binding.recyclerViewTabFriend.layoutManager = LinearLayoutManager(context)
        adapter = TabFriendAdapter(requireContext(), ArrayList())
        binding.recyclerViewTabFriend.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.resultFriendSort.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (adapter != null) {
                    adapter!!.setData(it)
                    adapter!!.setOnClick(object : TabFriendAdapter.IOnClick {
                        override fun onItemCLick(user: Users) {
                            viewModel.checkExistRoom(user)
                        }
                    })
                }
            }
        })
        viewModel.openChatRoom.observe(viewLifecycleOwner, Observer {
            it?.let {
                val bundle = Bundle()
                bundle.putSerializable("room", it)
                NavHostFragment.findNavController(this@TabFriendFragment).navigate(R.id.homeMessengerFragment_to_messengerFragment, bundle)
            }
        })
    }
}