package com.android.mvvm.chat.utils

import java.util.*

class Constants {
    companion object {
        const val BASE_URL = "https://api.github.com/"
        const val REQUEST_TIMEOUT_DURATION = 10
        const val DEBUG = true
        val USER_ID = "userid"
        val IMG_URL = "imgURL"
        val USER_NAME = "userName"
        val DEFAULT = "default"

        val LASTMES = "lastMes"
        val NAME = "name"
        val UNREAD = "unread"

        val USER_NAME_RES = "username"
        val IMG_RES = "imageURL"
        val ID_RES = "id"
        val USERS = "Users"
        val USER_RECEIVER = "userReceiver"
        val FRIENDS = "Friends"
        val CHATS = "Chats"

        val SENDER = "sender"
        val RECEIVER = "receiver"
        val MESSAGE = "message"
        val CHATLIST = "ChatList"
        val SEEN = "seen"

        val TIME_CHAT = "time"
        val ID = "id"
        val TOKENS = "Tokens"
        val CHAT_NULL = "Chat object is null"

        val API_STATUS_CODE_LOCAL_ERROR = 0

        val DB_NAME = "mindorks_mvp.db"
        val PREF_NAME = "mindorks_pref"

        val NULL_INDEX = -1L

        val SEED_DATABASE_OPTIONS = "seed/options.json"
        val SEED_DATABASE_QUESTIONS = "seed/questions.json"

        val TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss"

        val MY_READ_PERMISSION_CODE = 101

        val FRIEND_REQUEST = "friend_request"

        val REQUEST_RECEIVE = "request_receive"
        val REQUEST_SEND = "request_send"
        const val ROOMS = "Rooms"
        const val MEMBERS = "Members"
        const val MESSAGES = "Messages"


        object ReferencePath {
            const val USERS = "Users"
            const val ROOMS = "Rooms"
            const val MEMBERS = "Members"
            const val MESSAGES = "Messages"
            const val FRIENDS = "Friends"
            const val FRIEND_REQUEST = "friend_request"
            const val REQUEST_SEND = "request_send"
            const val REQUEST_RECEIVE = "request_receive"
        }

        object BundleKey {
            const val ROOM = "ROOM"
        }

        var stickers: List<String> = Arrays.asList(
            "https://fcbk.su/_data/stickers/daily_duncan_vol1/daily_duncan_vol1_02.png",
            "https://1.bp.blogspot.com/-03mAd23q9X4/Wpdf21tcU1I/AAAAAAALNHk/g_f7-GmXrW0CGu84ukzyl2HSo7x_3TLqwCLcBGAs/s1600/AS003723_02.gif",
            "https://i.pinimg.com/originals/c2/72/78/c272780f3e81f072283f9210e21c460c.jpg",
            "https://stickerly.pstatic.net/sticker_pack/GWE06IgDSetIKzU3GDJslg/X7AP86/20/cae7fcd9-4443-4fbb-a5fa-a0ba9a125d05.png",
            "https://i.pinimg.com/originals/da/29/ed/da29ede8b37b3169bd94b76e97ba492c.jpg",
            "https://i.pinimg.com/236x/ec/3c/f0/ec3cf0f10360168f982aa9de43d452f5.jpg",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_10.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_02.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_07.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_08.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_19.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_10.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_11.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_12.png",
            "https://fcbk.su/_data/stickers/playing_with_drogo/playing_with_drogo_13.png"
        )

        object MapKey {
            const val UNREAD_MESSAGE = "unreadMessage"
        }

    }
    enum class StateUser {
        FRIEND, REQUEST_FRIEND, OTHERS
    }
    object Notification {
        const val USER = "user"
        const val ICON = "icon"
        const val TITLE = "title"
        const val BODY = "body"
        const val SENT = "sent"
    }

}