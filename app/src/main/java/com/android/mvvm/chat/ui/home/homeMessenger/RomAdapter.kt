package com.android.mvvm.chat.ui.home.homeMessenger

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.R
import com.android.mvvm.chat.utils.Constants
import com.android.mvvm.chat.utils.DateUtils
import com.android.mvvm.databinding.UserItemRowBinding
import com.ankit.trendinggit.model.Room
import com.bumptech.glide.Glide
import java.text.Format
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class RoomAdapter(private val context: Context, data: ArrayList<Room>) :
    RecyclerView.Adapter<RoomAdapter.ViewHolder>() {
    private var data: ArrayList<Room>

    interface OnItemClick {
        fun onItemClick(room: Room)
    }

    var listener: OnItemClick? = null
    fun setOnItemClickListener(listener: OnItemClick?) {
        this.listener = listener
    }

    fun setData(data: ArrayList<Room>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun getData(): ArrayList<Room> {
        return data
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            UserItemRowBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val room: Room = data[position]
        try {
            holder.bind(room, listener)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class ViewHolder(val binding: UserItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
        private fun convertTime(time: Long): String {
            val date = Date(time)
            val format: Format = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
            return format.format(date)
        }

        private val currentTime: String
            private get() {
                val tsLong = System.currentTimeMillis()
                return tsLong.toString()
            }

        @Throws(ParseException::class)
        private fun checktime(room: Room): String {
            val time = room.timeStamp?.let { convertTime(it.toLong()) }
            var dateTime: Date = SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(time)
            val dateUtils = DateUtils(Calendar.getInstance().time)
            val dateMess = DateUtils(dateTime)
            val month: String? = dateUtils.getMonth()
            val month2: String? = dateMess.getMonth()
            val date: String? = dateUtils.getDate()
            val date2: String? = dateMess.getDate()
            return if (month == month2) {
                if (date!!.toInt() - date2!!.toInt() == 1) {
                    context.getString(R.string.yesterday)
                } else if (date!!.toInt() - date2!!.toInt() > 1) {
                    dateMess.getYear()!!
                } else {
                    dateMess.getHour()!!
                }
            } else {
                dateMess.getYear()!!
            }
        }

        @Throws(ParseException::class)
        fun bind(room: Room, listener: OnItemClick?) {
            binding.txtName.text = room.name
            val typeBoldName = ResourcesCompat.getFont(context, R.font.lato_bold)
            binding.txtName!!.setTypeface(typeBoldName)
            if (room.unreadMessage > 0 && room.unreadMessage < 9) {
                binding.txtUnreadMessage!!.visibility = View.VISIBLE
                val typeBold = ResourcesCompat.getFont(context, R.font.lato_bold)
                binding.txtLastMessage!!.setTextColor(context.getColor(R.color.black))
                binding.txtLastMessage!!.setTypeface(typeBold)
                binding.txtUnreadMessage.text = java.lang.String.valueOf(room.unreadMessage)
            } else if (room.unreadMessage > 9) {
                binding.txtUnreadMessage!!.text = "9+"
            } else {
                binding.txtUnreadMessage!!.visibility = View.GONE
                val typeNormal = ResourcesCompat.getFont(context, R.font.lato_normal)
                binding.txtLastMessage!!.setTypeface(typeNormal)
                binding.txtLastMessage!!.setTextColor(context.getColor(R.color.gray))
            }
            binding.txtLastMessage.setText(room.lastMessage)
            binding.tvTime!!.text = checktime(room)
            if (room.thumbnail != null && !room.thumbnail.equals(Constants.DEFAULT)) {
                Glide.with(context).load(room.thumbnail).into(binding.imgCircleHomeMess)
            } else {
                Glide.with(context).load(R.mipmap.ic_launcher).circleCrop().into(binding.imgCircleHomeMess)
            }


            binding.itemHomeMessenger!!.setOnClickListener { view: View? ->
                listener?.onItemClick(room)
            }
        }

    }

    init {
        this.data = data
    }

}