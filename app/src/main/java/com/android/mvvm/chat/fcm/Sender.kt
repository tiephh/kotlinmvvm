package com.android.mvvm.chat.fcm

import com.android.mvvm.chat.fcm.Data

class Sender {
    var data: Data? = null
    var to: String? = null

    constructor(data: Data?, to: String?) {
        this.data = data
        this.to = to
    }

    constructor()
}