package com.android.mvvm.chat.ui.register

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.android.mvvm.R
import com.android.mvvm.chat.ui.login.LoginViewModel


import com.android.mvvm.databinding.FragmentRegisterBinding

class RegisterFragment: Fragment() {
    private lateinit var binding: FragmentRegisterBinding
    private lateinit var viewModel: RegisterViewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@RegisterFragment).get(RegisterViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener()
        observer()
        changeColorButton()
    }

    private fun changeColorButton() {
        binding.etUernameRes.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.etEmailRes.text!!.length > 0
                    && binding.etPassRes.text!!.length > 0
                    && binding.etUernameRes.text!!.length > 0
                    && binding.cbPolicy.isChecked
                ) {
                    binding.btnRegister.setBackgroundResource(R.drawable.button_custom_blue)
                } else {
                    binding.btnRegister.setBackgroundResource(R.drawable.button_custom)
                }
            }
        })
        binding.etEmailRes.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.etEmailRes.text!!.length > 0
                    && binding.etPassRes.text!!.length > 0
                    && binding.etUernameRes.text!!.length > 0
                    && binding.cbPolicy.isChecked
                ) {
                    binding.btnRegister.setBackgroundResource(R.drawable.button_custom_blue)
                } else {
                    binding.btnRegister.setBackgroundResource(R.drawable.button_custom)
                }
            }
        })
        binding.etPassRes.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable) {
                if (binding.etEmailRes.text!!.length > 0
                    && binding.etPassRes.text!!.length > 0
                    && binding.etUernameRes.text!!.length > 0
                    && binding.cbPolicy.isChecked
                ) {
                    binding.btnRegister.setBackgroundResource(R.drawable.button_custom_blue)
                } else {
                    binding.btnRegister.setBackgroundResource(R.drawable.button_custom)
                }
            }
        })
        binding.cbPolicy.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { _, _ ->
            if (binding.etEmailRes.text!!.length > 0
                && binding.etPassRes.text!!.length > 0
                && binding.etUernameRes.text!!.length > 0
                && binding.cbPolicy.isChecked
            ) {
                binding.btnRegister.setBackgroundResource(R.drawable.button_custom_blue)
            } else {
                binding.btnRegister.setBackgroundResource(R.drawable.button_custom)
            }
        })
    }

    private fun listener(){
        binding.btnBackLogin.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_login_fragment, null)
        }
        binding.btnRegister.setOnClickListener {
            var email: String = binding.etEmailRes.text.toString()
            var username: String = binding.etUernameRes.text.toString()
            var pass: String = binding.etPassRes.text.toString()
            viewModel.register(email, pass, username)
        }
    }
    private fun observer(){
        viewModel.resultData.observe(viewLifecycleOwner, Observer{
            if (it == true){
                NavHostFragment.findNavController(this).navigate(R.id.action_repoListFragment_to_testFragment, null)
            }
        })
    }
}