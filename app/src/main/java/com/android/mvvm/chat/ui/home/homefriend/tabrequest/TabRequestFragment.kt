package com.android.mvvm.chat.ui.home.homefriend.tabrequest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.databinding.FragmentTabRequestBinding

class TabRequestFragment : Fragment() {
    private lateinit var viewModel: TabRequestViewModel
    private lateinit var binding: FragmentTabRequestBinding

    private var adapterReceiver: ReceiverAdapter? = null
    private var adapterSender: SenderAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTabRequestBinding.inflate(inflater, container, false).apply {
            viewModel =
                ViewModelProviders.of(this@TabRequestFragment).get(TabRequestViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObservers()
        setListeners()
    }

    private fun setListeners() {
        initAdapter()
        viewModel.readSender()
        viewModel.readReceiver()
    }

    private fun initAdapter() {
        val linearLayoutManager = LinearLayoutManager(context)
        binding.rvRequestFriend.layoutManager = linearLayoutManager
        adapterReceiver = ReceiverAdapter(requireContext(), ArrayList())
        binding.rvRequestFriend.adapter = adapterReceiver
        val linearLayoutManager1 = LinearLayoutManager(context)
        binding.rvSendRequestFriend.layoutManager = linearLayoutManager1
        adapterSender = SenderAdapter(requireContext(), ArrayList())
        binding.rvSendRequestFriend.adapter = adapterSender
        adapterReceiver!!.setOnClickItem(object : ReceiverAdapter.IOnClickItem {
            override fun itemClick(users: Users?) {
                viewModel.agreeRequest(users!!)
            }

            override fun deleteItem(users: Users?) {
                viewModel.disagreeRequest(users!!)
            }
        })
        adapterSender!!.setOnClickItemSender(object : SenderAdapter.IOnClickItemSender {
            override fun itemClick(users: Users?) {
                viewModel.cancelRequest(users!!)
            }
        })
    }


    private fun setupObservers() {
        viewModel.resultSender.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapterSender!!.setData(it)
            }
        })
        viewModel.resultReceiver.observe(viewLifecycleOwner, Observer {
            it?.let {
                adapterReceiver!!.setData(it)
            }
        })
    }
}