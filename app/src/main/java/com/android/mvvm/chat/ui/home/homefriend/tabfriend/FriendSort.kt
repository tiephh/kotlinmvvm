package mvvm.chat.ui.home.homefriend.tabfriend

import com.android.mvvm.chat.database.firebase.Users

class FriendSort {
    var header: String? = null
    var users: Users? = null
    var isSection = false

    constructor() {}
    constructor(
        header: String?, users: Users?, isSection: Boolean
    ) {
        this.header = header
        this.users = users
        this.isSection = isSection
    }

}