package com.android.mvvm.chat.ui.home.homeMessenger.messenger

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.mvvm.R
import com.android.mvvm.chat.database.firebase.Message
import com.android.mvvm.chat.utils.Constants
import com.android.mvvm.chat.utils.DateUtils
import com.android.mvvm.databinding.*
import com.bumptech.glide.Glide
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MessengerAdapter(private val context: Context, data: ArrayList<Message>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var data: ArrayList<Message>
    private var uId: String? = null
    private var thumail: String? = null
    private val mContext: Context = context
    fun setImg(imageURL: String?) {
        this.thumail = imageURL
    }

    fun setData(data: ArrayList<Message>) {
        this.data = data
        notifyDataSetChanged()
    }
    fun setUid(uId: String?){
        this.uId = uId
    }

    @Throws(ParseException::class)
    fun getTime(pos: Int): String? {
        val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val dateTime = sdf.parse(data[pos].time)

        val currentTime = DateUtils(Calendar.getInstance().time)
        val messTime = DateUtils(dateTime)

        val result = currentTime.getYear()
        val time = messTime.getYear()

        val month = messTime.getMonth()
        val month2 = currentTime.getMonth()

        val date = messTime.getDate()
        val date2 = currentTime.getDate()
        return if (month == month2) {
            if (date!!.toInt() - date2!!.toInt() == 1) {
                context.getString(R.string.yesterday)
            } else if (date!!.toInt() - date2!!.toInt() > 1) {
                messTime.getYear()
            } else if (date!!.toInt() - date2!!.toInt() == 0) {
                context.getString(R.string.today)
            } else {
                context.getString(R.string.today)
            }
        } else if (result == time) {
            context.getString(R.string.today)
        } else {
            messTime.getYear()
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when(viewType){
            MSG_RIGHT -> ViewHolderChatItemRight(ChatItemRightBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
            MSG_LEFT -> ViewHolderChatItemLeft(ChatItemLeftBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
            MSG_LEFT_IMG -> ViewHolderChatImgLeft(ChatItemImgLeftBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
            MSG_RIGHT_IMG -> ViewHolderChatImgRight(ChatItemImgRightBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val message: Message = data[position]
        when (holder){
            is ViewHolderChatItemRight -> holder.bind(position, message, mContext, data, thumail)
            is ViewHolderChatItemLeft ->holder.bind(position,message, mContext, data, thumail)
            is ViewHolderChatImgRight -> holder.bind(position,message, mContext, data, thumail)
            is ViewHolderChatImgLeft ->holder.bind(position,message, mContext, data, thumail)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolderChatItemRight(val binding: ChatItemRightBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            position: Int,
            message: Message,
            mContext: Context,
            data: ArrayList<Message>,
            thumail: String?
        ) {
            val date1 = SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(message.time)
            val hour = SimpleDateFormat("HH:mm")
            val time = hour.format(date1)
            binding.chatMessage.text = message.content
            if(position == data.size - 1){
                binding.seen.text = time
                binding.seen.visibility = View.VISIBLE
            }else{
                binding.seen.visibility = View.GONE
            }
        }
    }
    class ViewHolderChatItemLeft(val binding: ChatItemLeftBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            position: Int,
            message: Message,
            mContext: Context,
            data: ArrayList<Message>,
            thumail: String?
        ) {
            val date1 = SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(message.time)
            val hour = SimpleDateFormat("HH:mm")
            val time = hour.format(date1)
            binding.chatMessage.text = message.content
            Glide.with(mContext).load(
                if (thumail
                        .equals(Constants.DEFAULT)
                ) R.drawable.grapefruit else thumail
            )
                .into(binding.imageChatting)
            if(position == data.size - 1){
                binding.seen.text = time
                binding.seen.visibility = View.VISIBLE
            }else{
                binding.seen.visibility = View.GONE
            }
        }
    }
    class ViewHolderChatImgRight(val binding: ChatItemImgRightBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            position: Int,
            message: Message,
            mContext: Context,
            data: ArrayList<Message>,
            thumail: String?
        ) {
            val date1 = SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(message.time)
            val hour = SimpleDateFormat("HH:mm")
            val time = hour.format(date1)
            if(position == data.size - 1){
                binding.seen.text = time
                binding.seen.visibility = View.VISIBLE
            }else{
                binding.seen.visibility = View.GONE
            }
            Glide.with(mContext).load(message.content).into(binding.chatImg)
        }
    }
    class ViewHolderChatImgLeft(val binding: ChatItemImgLeftBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            position: Int,
            message: Message,
            mContext: Context,
            data: ArrayList<Message>,
            thumail: String?
        ) {
            val date1 = SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(message.time)
            val hour = SimpleDateFormat("HH:mm")
            val time = hour.format(date1)
            if(position == data.size - 1){
                binding.seen.text = time
                binding.seen.visibility = View.VISIBLE
            }else{
                binding.seen.visibility = View.GONE
            }
            Glide.with(mContext).load(
                if (thumail
                        .equals(Constants.DEFAULT)
                ) R.drawable.grapefruit else thumail
            )
                .into(binding.imageChatting)
            Glide.with(mContext).load(message.content).into(binding.chatImg)
        }
    }
    override fun getItemViewType(position: Int): Int {
        return if (data[position].senderId.equals(uId)) {
            if (data[position].content?.startsWith("https://") == true) {
                MSG_RIGHT_IMG
            } else {
                MSG_RIGHT
            }
        } else if (data[position].content?.startsWith("https://") == true) {
            MSG_LEFT_IMG
        } else {
            MSG_LEFT
        }
    }

    companion object {
        const val MSG_LEFT = 0
        const val MSG_RIGHT = 1
        const val MSG_LEFT_IMG = 2
        const val MSG_RIGHT_IMG = 3
    }

    init {
        this.data = data
    }

}