package com.android.mvvm.chat.ui.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.mvvm.databinding.FragmentTestBinding
import com.ankit.trendinggit.model.Room
import java.io.Serializable

class TestFragment : Fragment() {

    private lateinit var viewModel: TestViewModel
    private lateinit var binding: FragmentTestBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTestBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@TestFragment).get(TestViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObservers()
        setListeners()
        var room: Room = arguments?.getSerializable("room") as Room
        binding.tvResult.text = room.name
    }

    private fun setListeners() {
        binding.btnMinus.setOnClickListener {
            viewModel.minusResult()
        }

        binding.btnPlus.setOnClickListener {
            viewModel.plusResult()
        }
    }

    private fun setupObservers() {
        viewModel.resultData.observe(viewLifecycleOwner, Observer {
            it?.let {
                binding.tvResult.text = it.toString()
            }
        })
    }
}