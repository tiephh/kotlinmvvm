package com.android.mvvm.chat.ui.home.homefriend

import androidx.lifecycle.MutableLiveData
import com.android.mvvm.chat.base.BaseViewModel
import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.utils.Constants
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.ktx.Firebase
import java.util.ArrayList

class HomeFriendViewModel : BaseViewModel() {

    val countFriend = MutableLiveData<Int>()

    fun getCountFriend(){
        val userArrayList: ArrayList<Users> = ArrayList<Users>()
        val user = Firebase.auth.currentUser
        user?.let {
            FirebaseDatabase.getInstance().getReference(Constants.FRIEND_REQUEST).child(it.uid)
                .child(Constants.REQUEST_RECEIVE)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        userArrayList.clear()
                        for (dataSnapshot in snapshot.getChildren()) {
                            val user: Users? = dataSnapshot.getValue(Users::class.java)
                            if (user != null) {
                                if (user.id?.equals(it) == true) {
                                    userArrayList.add(user)
                                }
                            }
                        }
                        countFriend.postValue(userArrayList.size)
                    }
                    override fun onCancelled(error: DatabaseError) {

                    }
                })
        }
    }
}