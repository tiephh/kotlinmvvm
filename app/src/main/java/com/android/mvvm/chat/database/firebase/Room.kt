package com.ankit.trendinggit.model

import com.google.firebase.database.Exclude
import java.io.Serializable
import java.util.*

class Room : Serializable {
    var id: String? = null
    var name: String? = null
    var thumbnail: String? = null
    var receiver: String? = null
    var unreadMessage = 0
    var lastMessage: String? = null
    var timeStamp: String? = null

    constructor() {}
    constructor(
        id: String?,
        name: String?,
        thumbnail: String?,
        receiver: String?,
        unreadMessage: Int,
        lastMessage: String?,
        timeStamp: String?
    ) {
        this.id = id
        this.name = name
        this.thumbnail = thumbnail
        this.receiver = receiver
        this.unreadMessage = unreadMessage
        this.lastMessage = lastMessage
        this.timeStamp = timeStamp
    }

    @Exclude
    fun toMap(): Map<String, Any?> {
        val result = HashMap<String, Any?>()
        result["id"] = id
        result["name"] = name
        result["thumbnail"] = thumbnail
        result["unreadMessage"] = unreadMessage
        result["lastMessage"] = lastMessage
        result["receiver"] = receiver
        result["timeStamp"] = timeStamp
        return result
    }
}