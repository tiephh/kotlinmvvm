package mvvm.chat.ui.home.homefriend.tabuser

import com.android.mvvm.chat.database.firebase.Users
import com.android.mvvm.chat.utils.Constants


class UserSort {
    var header: String? = null
    var users: Users? = null
    var isSection = false
    var stateUser: Constants.StateUser? = null

    constructor() {}
    constructor(
        header: String,
        users: Users?,
        isSection: Boolean,
        stateUser: Constants.StateUser?
    ) {
        this.header = header
        this.users = users
        this.isSection = isSection
        this.stateUser = stateUser
    }

    fun isFriend(): Boolean {
        return stateUser!! == Constants.StateUser.FRIEND
    }

    fun isRequestFriend(): Boolean {
        return stateUser!! == Constants.StateUser.REQUEST_FRIEND
    }
}