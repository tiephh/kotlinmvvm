package com.android.mvvm.chat.ui.home.homeMessenger.messenger

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.mvvm.R
import com.android.mvvm.chat.base.BaseFragment
import com.android.mvvm.chat.utils.Constants
import com.android.mvvm.databinding.MessengerFragmentBinding
import com.ankit.trendinggit.model.Room
import com.bumptech.glide.Glide


class MessengerFragment: BaseFragment() {
    private lateinit var binding: MessengerFragmentBinding
    private val viewModel: MessengerViewModel by viewModels()
    private lateinit var adapter: MessengerAdapter
    private lateinit var photoAdapter: PhotoAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MessengerFragmentBinding.inflate(inflater, container, false).apply {
            //viewModel = ViewModelProviders.of(this@MessengerFragment).get(MessengerViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    //    val room: Room = arguments?.getSerializable("room") as Room
        viewModel.getDataSender()
        viewModel.room.observe(viewLifecycleOwner){room->
            setUpView(room)
            setUpObserver(room)
            initAdapter(room)
        }

        checkKeyBroad()
    }

    private fun checkKeyBroad() {
        if (view !is EditText) {
            requireView().setOnTouchListener { _, _ ->
                hideSoftKeyboard(requireActivity())
                false
            }
        }
    }

    private fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    private fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun setUpObserver(room: Room) {
        viewModel.getListMessage(room)

        viewModel.listMessage.observe(viewLifecycleOwner, Observer {
            adapter.setData(it)
        })
        viewModel.uid.observe(viewLifecycleOwner, Observer {
            adapter.setUid(it)
        })
    }

    private fun initAdapter(room: Room) {
        context?.let {
            adapter = MessengerAdapter(it, ArrayList())
        }
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.stackFromEnd = true
        binding.chatRecycleview.layoutManager = linearLayoutManager
        binding.chatRecycleview.adapter = adapter
        adapter.setImg(room.thumbnail)

    }

    private fun setUpView(room: Room) {
        binding.txtUsernameMessenger.text = room.name
        context?.let {
            Glide.with(it).load(
                    if (room.thumbnail
                            .equals(Constants.DEFAULT)
                    ) R.drawable.grapefruit else room.thumbnail
                )
                .into(binding.imgCircleMessenger)
        }
        binding.imgBackMessenger.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
        if (context != null) {
            if (room.thumbnail.equals(Constants.DEFAULT)) {
                Glide.with(requireContext()).load(R.mipmap.ic_launcher).circleCrop().into(binding.imgCircleMessenger)
            } else {
                Glide.with(requireContext()).load(room.thumbnail).circleCrop().into(binding.imgCircleMessenger)
            }
        }

        binding.btnGallery.setOnClickListener {
            checkPermission()
            binding.ctLayoutSticker.visibility = View.GONE
            hideKeyboard()
            if (binding.ctLayout.visibility == View.VISIBLE) {
                binding.ctLayout.visibility = View.GONE
                binding.btnGallery.setImageResource(R.drawable.ic_photo_1)
            } else {
                binding.ctLayout.visibility = View.VISIBLE
                binding.btnGallery.setImageResource(R.drawable.ic_add_photo)
            }
        }

        binding.btnSendImg.setOnClickListener{
            viewModel.sendImage(photoAdapter.getListImg(), room, context)
            photoAdapter.clearList()
            binding.btnSendImg.visibility = View.GONE
            binding.btnCancle.visibility = View.GONE
        }

        binding.btnCancle.setOnClickListener {
            binding.ctLayout.visibility = View.GONE
            binding.btnCancle.visibility = View.GONE
            binding.btnSendImg.visibility = View.GONE
            photoAdapter.clearList()
        }

        binding.btnSticker.setOnClickListener(View.OnClickListener {
            if (binding.ctLayoutSticker.visibility == View.VISIBLE) {
                binding.ctLayoutSticker.visibility = View.GONE
                binding.btnSticker.setImageResource(R.drawable.ic_smile_1)
            } else {
                binding.ctLayoutSticker.visibility = View.VISIBLE
                binding.btnSticker.setImageResource(R.drawable.ic_smile)
            }
            binding.ctLayout.visibility = View.GONE
            loadSticker(room)
            hideKeyboard()
        })
        sendMessage(room)
    }

    private fun loadSticker(room: Room) {
        val gridLayoutManager = GridLayoutManager(context, 3, LinearLayoutManager.VERTICAL, false)
        binding.gvSticker.layoutManager = gridLayoutManager
        binding.gvSticker.isFocusable = false
        val arrayList = ArrayList<String>()
        arrayList.addAll(Constants.stickers)
        val stickerAdapter = context?.let { StickerAdapter(it, arrayList) }
        stickerAdapter?.setOnClick(object : StickerAdapter.IOnClick{
            override fun itemClick(name: String?) {
                binding.ctLayoutSticker.visibility = View.GONE
                name?.let { viewModel.sendSticker(it, room, context) }
            }

        })
        binding.gvSticker.adapter = stickerAdapter
    }

    private fun checkPermission() {
        if (context?.let {
                ContextCompat.checkSelfPermission(it, Manifest.permission.READ_EXTERNAL_STORAGE)
            } != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions((context as Activity?)!!,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                Constants.MY_READ_PERMISSION_CODE
            )
        } else {
            loadImages()
        }
    }

    private fun loadImages() {
        val gridLayoutManager = GridLayoutManager(context, 3, LinearLayoutManager.VERTICAL, false)
        binding.gvGalley.layoutManager = gridLayoutManager
        binding.gvGalley.isFocusable = false
        val list: java.util.ArrayList<String>? = activity?.let { ImageGallery.lisOfImages(it) }
        photoAdapter = context?.let { PhotoAdapter(it, list) }!!
        photoAdapter.setOnClick(object : PhotoAdapter.IOnClick {
            override fun itemClick(name: String?) {
                if (photoAdapter.getListImg().size > 0) {
                    binding.btnCancle.visibility = View.VISIBLE
                    binding.btnSendImg.visibility = View.VISIBLE
                } else {
                    binding.btnCancle.visibility = View.GONE
                    binding.btnSendImg.visibility = View.GONE
                }
            }
        })
        binding.gvGalley.adapter = photoAdapter
    }
    private fun sendMessage(room: Room) {
        binding.imgSend.setOnClickListener {
            viewModel.sendMessage(binding.edtSend.text.toString(), room, context)
            binding.edtSend.text.clear()
        }
    }
}