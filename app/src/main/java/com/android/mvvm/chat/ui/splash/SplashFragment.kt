package com.android.mvvm.chat.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.databinding.SplashFragmentBinding

class SplashFragment: Fragment() {
    private lateinit var binding: SplashFragmentBinding
    private lateinit var viewModel: SplashViewModel
    val OPEN_LOGIN: Int = 1
    val OPEN_HOME: Int = 2
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = SplashFragmentBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@SplashFragment).get(SplashViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkUser()
    }

    private fun checkUser() {
        Handler(Looper.getMainLooper()).postDelayed(object : Runnable {
            override fun run() {
                viewModel.checkLogin()
            }
        },500)
        viewModel.checkUser.observe(viewLifecycleOwner, Observer {
            if(it == OPEN_LOGIN){
                NavHostFragment.findNavController(this).navigate(R.id.splash_to_login, null)
            }else{
                NavHostFragment.findNavController(this).navigate(R.id.splash_to_home, null)
            }
        })
    }
}