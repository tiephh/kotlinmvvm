package com.android.mvvm.chat.ui.home.homeprofile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.android.mvvm.R
import com.android.mvvm.chat.utils.Constants
import com.android.mvvm.databinding.FragmentHomeProfileBinding
import com.bumptech.glide.Glide

class HomeProfileFragment : Fragment() {

    private lateinit var viewModel: HomeProfileViewModel
    private lateinit var binding: FragmentHomeProfileBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeProfileBinding.inflate(inflater, container, false).apply {
            viewModel = ViewModelProviders.of(this@HomeProfileFragment)
                .get(HomeProfileViewModel::class.java)
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObservers()
        setListeners()
        viewModel.showProfile()
    }

    private fun setListeners() {
        binding.imgChangeProfile.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_homeFragment_to_changeProfileFragment, null)
        }
        binding.txtSignOut.setOnClickListener {
            alertDialog()
        }
        binding.changeLanguage.setOnClickListener {
//            showChangeLanguageDialog()
        }
        binding.languageCurrent.setOnClickListener {
//            showChangeLanguageDialog()
        }
    }

    private fun alertDialog() {
        androidx.appcompat.app.AlertDialog.Builder(requireContext())
            .setTitle(R.string.signout)
            .setMessage(R.string.signout)
            .setPositiveButton(
                android.R.string.yes
            ) { _, _ -> signOut() }
            .setNegativeButton(android.R.string.no, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun signOut() {
        viewModel.signOut()
        NavHostFragment.findNavController(this)
            .navigate(R.id.action_homeFragment_to_loginFragment, null)
    }

    private fun setupObservers() {
        viewModel.resultEmail.observe(viewLifecycleOwner, Observer {
            it?.let {
                binding.txtEmailProfile.text = it
            }
        })
        viewModel.resultUser.observe(viewLifecycleOwner, Observer {
            it?.let {
                val typeBold = ResourcesCompat.getFont(requireContext(), R.font.lato_bold)
                binding.txtNameProfile.typeface = typeBold
                binding.txtNameProfile.text = it.username
                if (context != null) {
                    if (it.imageURL.equals(Constants.DEFAULT)) {
                        Glide.with(requireContext()).load(R.mipmap.ic_launcher).into(binding.imgCircleProfile)
                        Glide.with(requireContext()).load(R.mipmap.ic_launcher).circleCrop().into(binding.imgProfile)
                    } else {
                        Glide.with(requireContext()).load(it.imageURL).circleCrop().into(binding.imgCircleProfile)
                        Glide.with(requireContext()).load(it.imageURL).circleCrop().into(binding.imgProfile)
                    }
                }
            }
        })
    }
}